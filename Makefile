JC = javac
JVM= java

CLASSPATH = classes/
SOURCEPATH = src/anagram/
BINPATH = bin/

MAIN = anagram.Main
MFFILE = MANIFEST.MF
JARFILE = anagram.jar

build:
	$(JC) -g -d $(CLASSPATH) -cp $(CLASSPATH) $(SOURCEPATH)*.java

run:
	$(JVM) -cp $(CLASSPATH) $(MAIN)

tojar: build
	mkdir -p $(BINPATH)
	jar -cvfm $(BINPATH)$(JARFILE) $(MFFILE) -C $(CLASSPATH) .

runjar:
	$(JVM) -jar $(BINPATH)$(JARFILE)

inspectjar:
	jar tf $(BINPATH)$(JARFILE)
