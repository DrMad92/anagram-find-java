package anagram;

import java.io.*;
import java.util.concurrent.TimeUnit;
import java.util.Arrays;

public class Main{
    public static void main(String[] args) {
        long startTime = System.nanoTime();
        
        String result = "";
        String encoding = "windows-1257";
        String file = args[0]; //Dictionary file
        String inputWord = args[1]; //Search word

        int inputWordLen = inputWord.length();

        Anagram ana = new Anagram(inputWord);
        BufferedReader reader = null;

        try {
            reader = new BufferedReader(new InputStreamReader(new FileInputStream(file), encoding));
             
            while(reader.ready()) {
                String line = reader.readLine();
                // System.out.println(line);
                if (inputWordLen != line.length()){
                    continue;
                }
                if (ana.isAnagram(line)){
                    result = result + "," + line;
                }
            }
        }
        catch (FileNotFoundException e) {
            System.out.println("File not found" + e);
        }
        catch (IOException ioe) {
            System.out.println("Exception while reading file " + ioe);
        }
        finally {
            try {
                if (reader != null) {
                    reader.close();
                }
            }
            catch (IOException ioe) {
                System.out.println("Error while closing stream: " + ioe);
            }
        }
        long stop = System.nanoTime() - startTime;
        System.out.println(TimeUnit.MICROSECONDS.convert(stop, TimeUnit.NANOSECONDS) + result);
    }
}