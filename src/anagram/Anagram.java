package anagram;

import java.util.HashMap;
import java.util.Map;

public class Anagram {
    private String word;
    private Map<Character, Integer> hash = new HashMap<Character, Integer>();

    public Anagram(String inputWord){
        this.word = inputWord;
        this.createHash(hash, this.word);
    }

    public boolean isAnagram(String compareWord){
        Map<Character, Integer> compareHash = new HashMap<Character, Integer>();
        this.createHash(compareHash, compareWord);

        return hash.equals(compareHash);
    }

    private void createHash(Map<Character, Integer> hash, String word) {
        word = word.toLowerCase();
        for (int i = 0; i < word.length(); i++) {
            Character c = word.charAt(i);
            
            if (!hash.containsKey(c)){
                hash.put(c, 1);
            }
            else {
                hash.put(c, hash.get(c) + 1);
            }
            
        }
    }
}