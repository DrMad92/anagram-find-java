# anagram-find-java

Find anagrams from dictionary

## Usage

`java -jar bin/anagram.jar <dictionary> <word>`
